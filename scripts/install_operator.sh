# Set the release version variable
RELEASE_VERSION=v1.25.0
# Linux download packages
curl -LO https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/operator-sdk_linux_amd64
curl -LO https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/ansible-operator_linux_amd64
curl -LO https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/helm-operator_linux_amd64

# Install on Linux
chmod +x operator-sdk_linux_amd64 && sudo mkdir -p /usr/local/bin/ && sudo cp operator-sdk_linux_amd64 /usr/local/bin/operator-sdk && rm operator-sdk_linux_amd64
chmod +x ansible-operator_linux_amd64 && sudo mkdir -p /usr/local/bin/ && sudo cp ansible-operator_linux_amd64 /usr/local/bin/ansible-operator && rm ansible-operator_linux_amd64
chmod +x helm-operator_linux_amd64 && sudo mkdir -p /usr/local/bin/ && sudo cp helm-operator_linux_amd64 /usr/local/bin/helm-operator && rm helm-operator_linux_amd64


